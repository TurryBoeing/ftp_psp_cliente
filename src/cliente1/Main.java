package cliente1;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 *
 * @author Cliente
 */
public class Main {

    private static String IP = "";
    private static final int PUERTOCONTROL = 2021;
    private static final int PUERTODATOS = 2047;
    private static int leidoServidor;
    private static int tamanhoArchivo;
    private static String servidorDijo = "";
    private static OutputStream salidaFichero;
    private static DataOutputStream salidaDatosFichero;

    public static void main(String[] args) {
        System.out.println("Introduce la IP a la cual conectarte ");
        Scanner indatos = new Scanner(System.in);
        IP = indatos.next();
        try {
            Socket conexionControl = new Socket(IP, PUERTOCONTROL);
            DataInputStream datosServidorControl = new DataInputStream(conexionControl.getInputStream());
            DataOutputStream salidaDatosControl = new DataOutputStream(conexionControl.getOutputStream());
            while (conexionControl.isConnected()) {
                if (conexionControl.isClosed()) {
                    break;
                }
                servidorDijo = datosServidorControl.readUTF();
                System.out.println("Servidor dice: " + servidorDijo);
                if ("Iniciando la transferencia de Invierno... ".equals(servidorDijo)) {
                    Socket conexionDatos = new Socket(IP, PUERTODATOS);
                    DataInputStream datosServidorDatos = new DataInputStream(conexionDatos.getInputStream());
                    salidaFichero = new FileOutputStream("C://FTPAnonimo//Invierno.jpg", true);
                    salidaDatosFichero = new DataOutputStream(salidaFichero);
//                    while (leidoServidor!=900) {
//                        leidoServidor = datosServidorDatos.read();
//                        salidaDatosFichero.write(leidoServidor);
//                        System.out.println(leidoServidor);
//                    }
//                    System.out.println("Salió");
                    for (int i = 0; i < tamanhoArchivo; i++) {
                        leidoServidor = datosServidorDatos.read();
                        salidaDatosFichero.write(leidoServidor);
                        System.out.println(leidoServidor);
                    }
                    salidaFichero.close();
                    salidaDatosFichero.close();
                    System.out.println("Fin de transferencia");
                    conexionDatos.close();
                }
                if ("Iniciando la transferencia de Prueba... ".equals(servidorDijo)) {
                    Socket conexionDatos = new Socket(IP, PUERTODATOS);
                    DataInputStream datosServidorDatos = new DataInputStream(conexionDatos.getInputStream());
                    salidaFichero = new FileOutputStream("C://FTPAnonimo//prueba.txt", true);
                    salidaDatosFichero = new DataOutputStream(salidaFichero);
                    for (int i = 0; i < tamanhoArchivo; i++) {
                        leidoServidor = datosServidorDatos.read();
                        salidaDatosFichero.write(leidoServidor);
                        System.out.println(leidoServidor);
                    }
                    salidaFichero.close();
                    salidaDatosFichero.close();
                    System.out.println("Fin de transferencia");
                    conexionDatos.close();                    
                }
                if ("Enviando el tamaño del archivo".equals(servidorDijo)) {
                    tamanhoArchivo = Integer.valueOf(datosServidorControl.readUTF());
                    System.out.println("Tamaño del archivo " + tamanhoArchivo);
                }
                if ("Puedes hablar... ".equals(servidorDijo)) {
                    System.out.println("Enviar mensaje a servidor: ");
                    salidaDatosControl.writeUTF(indatos.next());
                }
            }
        } catch (UnknownHostException ex) {
            System.out.println("Host desconocido " + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("Error I/O " + ex.getMessage());
        }
    }
}